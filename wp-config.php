<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'suusort-project' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', '123456' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'kC2NiD5DaxOJlpUP;6Sog&~aow([hWxj_qU}*;*#Afuv(hesm^#l?wDm;$W@r@Wm' );
define( 'SECURE_AUTH_KEY',  'ki=v]|9$fWz:G@i35<=V7w?8/&vr#mWv[v=maPoL gspWG!3+ShUk9FSP.MY@_=G' );
define( 'LOGGED_IN_KEY',    '_!e=l,tEqrqe2_Xj>LdJ3tgV3BYScZAol5DE,hBb4Gf]=g[UDJI3izlcphY f#pH' );
define( 'NONCE_KEY',        ')R;N#p+=LIslNj[A/Z,Z;zSf7;Hc6lUf}a0R%0c?[&rLyFLbxWi2B8S^uP8<^ku^' );
define( 'AUTH_SALT',        'b}5{oMB_F=x7SoD]e(y}DC:-F<wpE1?l>W%;~ZcZ=Kvyd%%HWUN?RwJ0npUALjSw' );
define( 'SECURE_AUTH_SALT', 'U_:yAerfzlJhbS)E%9N/E>I;9g?;+9kE=Ur(~;h!@RKg%j?cfwcsy(G]}s2V]qf[' );
define( 'LOGGED_IN_SALT',   'q*|M[y3_c8jl0YFf~ZU07[&b:,O%Z1H;;Diyd*,uD:,qc`yG!X?`cw@3~S9J}D<=' );
define( 'NONCE_SALT',       'n|:<pCL5W6ipj2fH~qMD#`lpR;%u^p:~08CI>6^HSyrj$}5hoj]4H<P]K;p&IkHk' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
